import { ApiPromise, WsProvider } from '@polkadot/api'
import { mnemonicGenerate, mnemonicValidate, mnemonicToLegacySeed, hdEthereum } from '@polkadot/util-crypto'
import { Keyring } from '@polkadot/keyring'
import { decodeAddress, encodeAddress } from '@polkadot/keyring'
import { hexToU8a, isHex, u8aToHex } from '@polkadot/util'
import { BN } from "bn.js/lib/bn.js";

export const connect = async () => {
    const wsProvider = new WsProvider('wss://westend-rpc.polkadot.io');
    const api = new ApiPromise({ provider: wsProvider });

    return api.isReady;
}

export const getMnemonic = () => {
    return mnemonicGenerate();
}

export const createAccount = (mnemonicUser) => {
    let mnemonic = mnemonicUser && mnemonicValidate(mnemonicUser)
        ? mnemonicUser
        : mnemonicGenerate();
    const keyring = new Keyring({ type: 'sr25519' });
    const account = keyring.addFromMnemonic(mnemonic);

    return { account, mnemonic };
}

export const isValidAddress = (address) => {
    try {
        encodeAddress(
            isHex(address)
                ? hexToU8a(address)
                : decodeAddress(address)
        );

        return true;
    } catch (error) {
        return false;
    }
}

export const transfer = async (mnemonicSender, amount, toAddress, api) => {

    const keyring = new Keyring({ type: 'sr25519' });
    const sender = keyring.createFromUri(mnemonicSender)

    if (!isValidAddress(sender.address)) {
        alert("Sender mnemonic invalid!");
        return
    }

    if (!isValidAddress(toAddress)) {
        alert("toAddress invalid!");
        return
    }

    const balance = await api.derive.balances.all(sender.address);
    const available = balance.availableBalance;
    const decims = new BN(api.registry.chainDecimals);
    const factor = new BN(10).pow(decims);
    const amountBN = new BN(amount).mul(factor);

    const transfer = api.tx.balances.transfer(toAddress, amountBN)

    const { partialFee } = await transfer.paymentInfo(sender);

    const fees = partialFee.muln(110).divn(100);

    const total = amountBN
        .add(fees)
        .add(api.consts.balances.existentialDeposit);

    if (total.gt(available)) {
        alert(
            `Cannot transfer ${amount} with ${available} left`
        )
    }
    else {
        const tx = await transfer.signAndSend(sender);
        alert(`Created transfer: ${tx}`)
    }
}
